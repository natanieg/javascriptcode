window.onload = function () 
{
    this.FizzBuzz(100);
}

function FizzBuzz(num)
{
    var fizzbuzzList = [];
    var displayList = document.getElementById("fizzbuzz");
    for (var i=1; i < num; i++)
    {
        if (i % 15 == 0) createNode(displayList, "FizzBuzz");
        else if (i % 3 == 0) createNode(displayList, "Fizz");
        else if (i % 5 == 0) createNode(displayList, "Buzz");
        else createNode(displayList, i);
    }
}

function createNode(parent, text)
{
    var li = document.createElement("li");
    li.appendChild(document.createTextNode(text));

    parent.appendChild(li);
}