# Assignment 1: FizzBuzz
- Link: https://gitlab.com/natanieg/javascriptcode/tree/master/task1

# Assignment text
Write a HTML website that displays the words Fizz and/or Buzz inside an unordered list element (<ul>). 

Put your JavaScript in a seperate file name "app.js" and include your JavaScript code using the <script> tag in the <body> of the HTML site.

Use the document  object in Javascript to select the parent element (The <ul>) based on an ID attribute. Each child element of the <ul> should be a list (<li>) element 

Instructions:

	- Write a function that accepts 1 parameter, that will be number of iterations.
 	- The function should check based on the conditions below
 	- The result must be inserted into the <ul> using JavaScript
 	- Remember the child elements must be a <li> element
 	- Start your loop at 1
 	- i.e. fizzbuzz(50) would iterate 50 times