import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss']
})
export class PokemonCardComponent implements OnInit {


  @Input() pokemon = {};
  @Output() goToPokemon: EventEmitter<number>;

  constructor() {
    this.goToPokemon = new EventEmitter();
  }

  ngOnInit() {
  }

  onViewMoreClicked(id: number) {
    this.goToPokemon.emit(id);
  }

}
