import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PokemonListPageComponent } from './pages/pokemon-list-page/pokemon-list-page.component';
import { PokemonDetailPageComponent } from './pages/pokemon-detail-page/pokemon-detail-page.component';


const routes: Routes = [
  {
    path: 'pokemon-cards',
    component: PokemonListPageComponent
  },
  {
    path: 'pokemon-detail/:id',
    component: PokemonDetailPageComponent
  },
  {
    path: '',
    redirectTo: '/pokemon-cards',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
