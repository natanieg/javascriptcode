import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokemonClientServiceService {

  constructor(private http: HttpClient) {

  }

  getPokemon(): Promise<any> {
    return this.http.get('https://pokeapi.co/api/v2/pokemon').toPromise();
  }

  getPokemonByUrl(url: string): Promise<any> {
    return this.http.get(url).toPromise();
  }

  getPokemonById(id: string): Promise<any> {
    return this.http.get(`https://pokeapi.co/api/v2/pokemon/${id}`).toPromise();
  }
}
