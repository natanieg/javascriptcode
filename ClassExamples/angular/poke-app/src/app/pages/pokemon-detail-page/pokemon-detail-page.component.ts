import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonClientServiceService } from 'src/app/services/pokemon-client-service.service';

@Component({
  selector: 'app-pokemon-detail-page',
  templateUrl: './pokemon-detail-page.component.html',
  styleUrls: ['./pokemon-detail-page.component.scss']
})
export class PokemonDetailPageComponent implements OnInit {

  @Input() pokemonDetail = {};

  constructor(private activeRoute: ActivatedRoute, private pokeClient: PokemonClientServiceService) {

  }

  async ngOnInit() {

    try {
      const id = this.activeRoute.snapshot.paramMap.get('id');
      this.pokemonDetail = await this.pokeClient.getPokemonById(id);
      console.log(this.pokemonDetail);

    } catch (error) {
      console.error(error);

    }
  }

}
