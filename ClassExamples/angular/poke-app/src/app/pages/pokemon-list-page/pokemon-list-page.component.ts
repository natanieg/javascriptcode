import { Component, OnInit } from '@angular/core';
import { PokemonClientServiceService } from 'src/app/services/pokemon-client-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-list-page',
  templateUrl: './pokemon-list-page.component.html',
  styleUrls: ['./pokemon-list-page.component.scss']
})
export class PokemonListPageComponent implements OnInit {

  pokemonCards = [];

  constructor(private router: Router, private pokeClient: PokemonClientServiceService) {

  }

  async ngOnInit() {

    try {
      const { results } = await this.pokeClient.getPokemon();
      //console.log(results);

      // Check if there are any results

      results.forEach(pokemon => {
        this.pokeClient.getPokemonByUrl(pokemon.url)
          .then(pokemonDetail => {
            this.pokemonCards.push(pokemonDetail);
          })
      });


    } catch (error) {
      console.error(error);

    }
  }

  onGoToPokemon(id) {
    // Navigate to new page using ID
    this.router.navigateByUrl(`/pokemon-detail/${id}`);

  }

}
