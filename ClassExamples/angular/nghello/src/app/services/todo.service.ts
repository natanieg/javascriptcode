import { Injectable } from '@angular/core';
import { TodoItem } from '../models/todo-item-model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  todo = {} as TodoItem;
  private todos: TodoItem[] = [];
  private todos$: BehaviorSubject<TodoItem[]> = new BehaviorSubject<TodoItem[]>([]);

  constructor() { }

  getTodos$(): Observable<TodoItem[]> {
    return this.todos$.asObservable();
  }

  addTodo(newTodo: TodoItem): void {

    //if (this.todo.title && this.todo.title.length > 0) {
    //  this.todo.completed = false;
    //  this.todo.created_at = Date.now().toString();
    //  //this.addTodo.emit(this.todo);
    //  this.todo = {} as TodoItem;
    //  this.todos.push(this.todo);
    //  this.todos$.next(this.todos);
    //}
    //else {
    //  alert("Please enter a todo first. ");
    //}


    newTodo.id = this.nextId();
    this.todos.push(newTodo);

    this.todos$.next(this.todos);
  }

  deleteTodo(id): void {
    this.todos = this.todos.filter((item) => {
      return item.id !== id;
    });
    this.todos$.next(this.todos);
  }

  updateTodo(todo: TodoItem) {
    for (let i = 0; i < this.todos.length; i++) {
      if (this.todos[i].id === todo.id) {
        this.todos[i] = todo;
        this.todos[i].edit = false;
        break;
      }
    }
    this.todos$.next(this.todos);
  }

  setEditable(id: number) {
    for (let i = 0; i < this.todos.length; i++) {
      if (this.todos[i].id === id) {
        this.todos[i].edit = true;
        break;
      }
    }
    this.todos$.next(this.todos);
  }

  private nextId(): number {
    return this.todos.length > 0 ? this.todos[this.todos.length - 1].id + 1 : 1;
  }
}
