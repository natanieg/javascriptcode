import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TodoItem } from 'src/app/models/todo-item-model';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.scss']
})
export class AddTodoComponent implements OnInit {

  @Output() addTodo: EventEmitter<TodoItem>;

  todo = {} as TodoItem;

  constructor(private todoService: TodoService) {
    this.addTodo = new EventEmitter<TodoItem>();
  }

  ngOnInit() {
  }

  onAddClicked() {
    //this.todoService.addTodo();

    if (this.todo.title && this.todo.title.length > 0) {
      this.todo.completed = false;
      this.todo.created_at = Date.now().toString();
      this.addTodo.emit(this.todo);
      this.todo = {} as TodoItem;
    }
    else {
      alert("Please enter a todo first. ");
    }
  }
}
