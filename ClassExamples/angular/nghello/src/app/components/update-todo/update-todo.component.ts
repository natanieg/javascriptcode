import { Component, OnInit, Input } from '@angular/core';
import { TodoItem } from 'src/app/models/todo-item-model';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-update-todo',
  templateUrl: './update-todo.component.html',
  styleUrls: ['./update-todo.component.scss']
})
export class UpdateTodoComponent implements OnInit {

  @Input() todo: TodoItem;

  constructor(private todoService: TodoService) { }

  ngOnInit() {
  }

  onUpdateClicked() {
    if (this.todo.title) {
      this.todoService.updateTodo(this.todo);
    }
  }

}
