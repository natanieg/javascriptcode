import { Component, OnInit, OnDestroy } from '@angular/core';
import { TodoItem } from 'src/app/models/todo-item-model';
import { TodoService } from 'src/app/services/todo.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit, OnDestroy {

  // Reference to the TodoService.todos
  todos: TodoItem[] = [];
  todos$: Subscription;

  constructor(private todoService: TodoService) {
  }

  onDeleteClicked(id: number) {
    this.todoService.deleteTodo(id);
  }

  onTitleClicked(id: number) {
    this.todoService.setEditable(id);
  }

  ngOnInit() {
    this.todos$ = this.todoService.getTodos$().subscribe(updatedTodos => {
      this.todos = updatedTodos;
    });
  }

  ngOnDestroy() {
    this.todos$.unsubscribe();
  }

}
