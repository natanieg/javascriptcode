# NODE + Express login

## Views (HTML files)

- login.html - Login form
- index.html - Home page (random text and a heading)
- about.html - photo, some random text
- contact.html - contact form (not really)

## Statics (css, js, images)

- css/styles.css - Some basic styling for our awesome site
- js/login.js - Handle the login
- img/about.jpg - Image

## Node + Express

### View routes

- '/' - login.html
- '/home' - index.html
- '/about' - about.html
- '/contact' - contact.html

### Other (misc.)

- '/auth' - [POST] - Validate login credentials (this will return -JSON)
