const btnLogin = document.getElementById('btn-login');

btnLogin.addEventListener('click', function () {
    const xhr = new XMLHttpRequest();

    xhr.open('POST', '/auth');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onreadystatechange = function () {

        if (xhr.readyState === 4) {
            // We recieved a response
            const response = JSON.parse(xhr.responseText);

            if (response.loggedin) {
                window.location.href = '/home';
            } else {
                alert(response.message);
            }
        }
    }

    const loginUser = {
        username: document.getElementById('username').value,
        password: document.getElementById('password').value
    }
    xhr.send(JSON.stringify(loginUser));
});