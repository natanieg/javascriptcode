const express = require('express');
const path = require('path');
const PORT = process.env.PORT || 3000
const session = require('express-session');

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/public', express.static(path.join(__dirname, 'static')));

app.use(session({
    secret: 'random',
    resave: true,
    saveUninitialized: false,
    cookie: {
        maxAge: 8 * 60 * 60 * 1000,
    }
}))


// -------------- View routes --------------------------------
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'views', 'login.html'));
})

app.get('/home', (req, res) => {
    if (req.session.loggedin) {
        res.sendFile(path.join(__dirname, 'views', 'index.html'));
    } else {
        res.redirect('/');
    }
})

app.get('/about', (req, res) => {
    res.sendFile(path.join(__dirname, 'views', 'about.html'));
})


app.get('/contact', (req, res) => {
    res.sendFile(path.join(__dirname, 'views', 'contact.html'));
})




// -------------- Request --------------------------------
app.post('/auth', (req, res) => {
    const { username, password } = req.body;
    if (username === 'name' && password === '123') {
        req.session.loggedin = true;
        req.session.username = username;

        res.json({
            loggedin: true
        }).end();
    }
    else {
        res.json({
            loggedin: false,
            message: 'Invalid username or password'
        }).end();
    }
})


app.listen(PORT, (e) => {
    if (e) {
        new Error(e);
    }
    console.log(`Server running on port: ${PORT}`);
})