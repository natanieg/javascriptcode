import React from 'react';
import { Route } from 'react-router-dom';
import CardList from './components/card-list/CardList';
import CardDetail from './components/CardDetail/CardDetail';

function App() {

    return (
        <React.Fragment>
            <Route path="/" exact component={CardList} />
            <Route path="/card-detail/:id" component={CardDetail} />
        </React.Fragment>



    );
}

export default App;