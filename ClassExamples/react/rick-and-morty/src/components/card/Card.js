import React from 'react'
import { Link } from 'react-router-dom';
import styles from './Card.module.css'


function Card(props) {

    const { card } = props;


    return (
        <div className={styles.card}>

            <img src={card.image} alt={card.name} />
            <h4>{card.name}</h4>
            <div className={styles.stats}>
                <p>Status: <span> {card.status}  </span></p>
                <p>Species: <span> {card.species}  </span></p>
                <p>Gender: <span>  {card.gender}  </span></p>
            </div>

            <Link to={`/card-detail/${card.id}`} className="button button-blue button-bordered">
                <button className="button--inner">View more</button>
            </Link>
            {/*<button onClick={() => props.goToCard()}>Click me!</button>*/}
        </div >
    );
}

export default Card;