import React from 'react';

// Functional/presentation component
function Header(props) {
    return (
        <header>
            <h1>Rick and Morty</h1>
        </header>
    )
}

export default Header;