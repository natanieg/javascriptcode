import React from 'react';
import styles from './CardDetail.module.css';

class CardDetail extends React.Component {
    state = {
        char: null
    }

    componentDidMount() {
        const charId = this.props.match.params.id;
        try {
            fetch(`https://rickandmortyapi.com/api/character/` + charId)
                .then(resp => resp.json())
                .then(resp => {
                    console.log(resp);
                    this.setState({
                        char: resp
                    })
                })

        } catch (error) {
            console.error(error);
        }
    }

    render() {
        let detailCard = null;
        let char = this.state.char;


        if (char) {
            if (char.type === "") {
                char.type = "No special type";
            }

            detailCard =
                <div className={styles.charDetails} >
                    < h2 > {char.name}</h2 >
                    <br />
                    <img src={char.image} alt={char.name} className={styles.detailImageChar}></img>
                    <br />
                    <div className={styles.statsDetailed}>
                        <p>Status:   <span> {char.status}        </span></p>
                        <p>Species:  <span> {char.species}       </span></p>
                        <p>Gender:   <span> {char.gender}        </span></p>
                        <p>Type:     <span> {char.type}          </span></p>
                        <p>Location: <span> {char.location.name} </span></p>
                        <p>Origin:   <span> {char.origin.name}   </span></p><br />
                    </div>

                </div>
        }
        else {
            detailCard = <p>Loading Rick And Morty Character details...</p>;
        }


        return (
            <React.Fragment>
                {detailCard}
            </React.Fragment>
        )
    }
}

export default CardDetail