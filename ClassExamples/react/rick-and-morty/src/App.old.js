import React from 'react';
import Header from './components/header/Header';

// Create a statefull component.
// A statefull component is usually a class

class App extends React.Component {

    state = {
        message: "Hello, The world is running fine. "
    };

    updateMessage() {
        this.setState({ message: "Oh no. The world has ended. " })
    }

    // Every class must have a render method
    render() {
        // The render method must return valid jsx
        return (
            <React.Fragment>
                <Header />
                <h1>Hello from the app</h1>
                <p>This is working</p>
                <p>{this.state.message}</p>
                <button onClick={() => this.updateMessage()}>    DOOOOOM  </button>
            </React.Fragment>
        );
    }
}

// remember to export the class
export default App;