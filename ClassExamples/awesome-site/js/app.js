const elOrderList = document.getElementById("order-list");
const elMenuItems = document.getElementById("menu");
const order = {
    items: [],
}

function onMenuChanged() {
    console.log("Menu changed. ");
}

function addToOrder() {
    const menuItem = elMenuItems.value;
    if (menuItem == -1) {
        alert("Please select something from the menu. ");
    }
    else {
        order.items.push(menuItem);
        updateOrder();
    }
    console.log(order);
}

function updateOrder() {
    elOrderList.innerHTML = "";

    if (order.items.length <= 0) {
        elOrderList.innerHTML = "<li>404: Food not found. </li>";
        return;
    }

    order.items.forEach(element => {
        const elItem = document.createElement("li");
        elItem.innerText = element;
        elOrderList.appendChild(elItem);
    });
}

function clearOrder() {
    order.items = "";
    updateOrder();
}