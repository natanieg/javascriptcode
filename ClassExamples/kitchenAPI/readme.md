# Kitchen API

- Use express
- Use express router
- Use a custom response object
- Implement Middleware - Use next
- Make use of ES6 classes
- Follow the module design - using exports

# The kitchen API

## Endpoints

- [GET] /api/v1/kitchen/appliances
- [POST] /api/v1/kitchen/appliances/add
- [PUT] /api/v1/kitchen/appliances/update/:id
- [DELETE] /api/v1/kitchen/appliances/delete/:id
