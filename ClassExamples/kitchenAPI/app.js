const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;
const kitchenRoutes = require('./routes/kitchen');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use((req, res, next) => {
    let authenticated = true;

    if (authenticated) {
        next();
    }
    else {
        res.json({
            error: 'Not authenticated'
        })
    }
})

app.use('/api/v1/kitchen', kitchenRoutes);

app.listen(PORT, () => {
    console.log(`Server running on port: ${PORT}`);
})
