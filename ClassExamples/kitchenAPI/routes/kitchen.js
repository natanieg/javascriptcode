const express = require('express');
const router = express.Router();
const kitchenAppliance = require('../models/kitchen-appliance')

router.get('/', (req, res) => {
    const apiRes = kitchenAppliance.getAppliances();
    res.json(apiRes).end();
})

router.post('/add', (req, res) => {
    res.send("Adding a kitchen appliance").end();
})

router.put('/update/:id', (req, res) => {
    res.send("Updating a kitchen appliance").end();
})

router.delete('/delete/:id', (req, res) => {
    res.send("Deleting a kitchen appliance").end();
})

module.exports = router;