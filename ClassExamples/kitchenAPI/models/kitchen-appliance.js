const apiResponse = require('./api-response');

class KitchenAppliances {


    getAppliances() {
        const apiRes = new apiResponse();
        apiRes.setData(['Kettle', 'Fridge', 'Bean Grinder', 'Air fryer']);
        return apiRes;
    }

    getAppliance(applianceID) {

    }

    addAppliance() {

    }

    updateAppliance(applianceID) {

    }

    deleteAppliance(applianceID) {

    }
}


module.exports = new KitchenAppliances();