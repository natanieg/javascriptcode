class ApiResponse {
    constructor() {
        this.data = null;
        this.success = true;
        this.error = '';
    }

    setData(data) {
        this.data = data;
    }

    setSuccess(success) {
        this.success = success;
    }

    setError(error) {
        this.error = error;
    }
}

module.exports = ApiResponse;