class Player {
    /**
     * Plays a warrior turn.
     *
     * @param {Warrior} warrior The warrior.
     */

    constructor() {
        this.health = 20;
        this.reachedWallBehind = false;
        this.enemiesInSight = false;
    }

    playTurn(warrior) {
        let look = warrior.look();

        if (warrior.feel().isEmpty()) {
            if (warrior.health() < (warrior.maxHealth())
                && warrior.health() >= this.health
                && !this.enemiesInSight) {
                warrior.rest();
            }
            else {
                this.enemiesInSight = false;
                for (let field of look) {
                    if (field.isUnit()) {
                        if (field.getUnit().isEnemy()) {
                            this.enemiesInSight = true;
                            if (warrior.health() != this.health
                                && warrior.feel().isEmpty()) {
                                warrior.walk();
                            }

                            else {
                                warrior.shoot();
                            }
                        }
                    }
                };
                if (!this.enemiesInSight) {
                    warrior.walk();
                }
            }
        }
        else if (warrior.feel().isWall()) {
            warrior.pivot();
            this.reachedWallBehind = true;
        }
        else if (warrior.feel().getUnit().isBound()) {
            warrior.rescue();
        }
        else if (warrior.feel().getUnit().isEnemy()) {
            warrior.attack();
        }
        this.health = warrior.health();
    }
}
