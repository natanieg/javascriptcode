{
    init: function(elevators, floors) {
        waitingQueue = [];

        elevators.forEach(elevator => {
            elevator.destinationQueue = [];

            elevator.on("floor_button_pressed", function (floorNum) {
                elevator.destinationQueue.push(floorNum);
            })

            elevator.on("idle", function () {

                if (elevator.destinationQueue[0]) {
                    elevator.goToFloor(elevator.destinationQueue.shift());

                    if (elevator.destinationDirection === "up") {
                        elevator.goingDownIndicator(false);
                        elevator.goingUpIndicator(true);
                    }
                    else if (elevator.destinationDirection === "down") {
                        elevator.goingDownIndicator(true);
                        elevator.goingUpIndicator(false);
                    }
                    // else {
                    //     elevator.goingDownIndicator(false);
                    //     elevator.goingUpIndicator(false);
                    // }
                }
                else if (waitingQueue[0]) {
                    // if ((elevator.goingUpIndicator
                    //     && elevator.currentFloor() <= waitingQueue[0])
                    //     ||(
                    //         elev<toString.goingDownIndicator
                    //         && elevator.currentFloor() >= waitingQueue[0]
                    //     )
                    //     && elevator.loadFactor() < 0.9) 
                    //     {
                    elevator.goToFloor(elevator.destinationQueue.shift());
                    //}
                }
                else {
                    elevator.goToFloor(0);
                }
            });

        });
        floors.forEach(floor => {
            floor.on("up_button_pressed", function () {
                var delegated = false;
                elevators.forEach(elevator => {

                    if (elevator.goingUpIndicator
                        && elevator.loadFactor() < 0.8
                        && !delegated) {
                        elevator.destinationQueue.unshift(floor.floorNum());
                        delegated = true;
                    }
                });
                if (!delegated) {
                    waitingQueue.push(floor.floorNum())
                    delegated = true;
                }
            })

            floor.on("down_button_pressed", function () {
                var delegated = false;
                elevators.forEach(elevator => {

                    if (elevator.goingDownIndicator
                        && elevator.loadFactor() < 0.8
                        && !delegated) {
                        elevator.destinationQueue.unshift(floor.floorNum());
                        delegated = true;
                    }

                });
                if (!delegated) {
                    waitingQueue.push(floor.floorNum())
                    delegated = true;
                }
            })
        });
    },
    update: function(dt, elevators, floors) {
        // We normally don't need to do anything here
    }
}