$(document).ready(function () {
    const gallery = new Gallery();
    gallery.getAlbum(function (data) {
        //console.log(data);
        //const firstPhoto = data[0];

        const $gallery = $('#gallery');

        $(data).each(function (i, image) {
            $gallery.append(`
            <a href="${image.url}" class="single_image" rel="gallery1">
                <img src="${image.thumbnailUrl}" alt="" class="single_image_thumb">
            </a>
        `);

        });



        // $('#single_image').attr('href', firstPhoto.url);
        // $('#single_image_thumb').attr('src', firstPhoto.thumbnailUrl);

        // $('#single_image').fancybox();
        $('a.single_image').fancybox();

    });
});

const Gallery = function () {
    return {
        getAlbum(completed) {
            $.ajax({
                url: 'https://jsonplaceholder.typicode.com/albums/1/photos',
                method: 'GET',
                success: function (data) {
                    const photos = data.slice(0, 50);
                    completed(photos);
                },
                error: function (error) {
                    console.log(error);

                }
            })


        }
    }
}