let $resultName = null;
let $resultEmail = null;
let $resultReason = null;
let $resultComment = null;

$(document).ready(function () {

    $resultName = $('#result-name');
    $resultEmail = $('#result-email');
    $resultReason = $('#result-reason');
    $resultComment = $('#result-comment');

    $('#name').keyup(function (event) {
        $resultName.text("Name: " + event.target.value);
    });

    $('#email').keyup(function (event) {
        $resultEmail.text("E-mail: " + event.target.value);
    });

    $('#reason').change(function (event) {
        if (event.target.value != -1) {
            $resultReason.text("Reason for contact: " + event.target.value);
        }
    });

    $('#comment').keyup(function (event) {
        $resultComment.text("User comment: " + event.target.value);
    });

    $('#btn-submit').click(function (event) {
        event.preventDefault();
    })
});;