// IIFE.
const postComments = (function () {
    // private

    //public
    return {
        async getComments() {
            const elComment = document.getElementById("comments");

            try {
                const response = await fetch("https://jsonplaceholder.typicode.com/comments")
                    .then(response => response.json());
                const postId = new URLSearchParams(window.location.search).get('post');

                response.filter((comment) => comment.postId == (postId))
                    .forEach(comment => {
                        elComment.appendChild(postComments.createCommentTemplate(comment));
                    });

            } catch (error) {
                console.error(error);
            }
        },
        createCommentTemplate(comment) {
            const commentDiv = document.createElement("div");
            const commentTitle = document.createElement("h4");
            const commentBody = document.createElement("p");

            commentTitle.innerText = comment.name;
            commentBody.innerText = comment.body;

            commentDiv.appendChild(commentTitle);
            commentDiv.appendChild(commentBody);

            return commentDiv;
        }
    }
})();

postComments.getComments();