const express = require('express');
const PORT = process.env.PORT || 3000;
const authMiddleware = require('./middleware/auth.middleware');

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(authMiddleware);

// ----------------- Routes -----------------


const authRoutes = require('./routes/auth.route');
const pokemonRoutes = require('./routes/pokemon.route');
app.use('/api/v1', authRoutes);
app.use('/api/v1', pokemonRoutes);





// ---------------- Requests ------------------
app.listen(PORT, (e) => {
    if (e) {
        new Error(e);
    }
    else {
        console.log(`The server has started on port: ${PORT}`);

    }
})