# Pokemon API with Postgress DB

BaseURL: /api/v1

## Endpoints

- [GET] /pokemon - Get a list of pokemon
- [POST] /pokemon/add - Add a new okemon
- [PUT] /pokemon/update/:id - Update pokemon based on ID
- [DELETE] /pokemon/delete/:id - Set active flag on the pokemon to 0

## Models

- Database - a class to manage our connection to postgress
- Auth - a class to manage API tokens
- pokemon - a class that manages all pokemon data
- APIResponse - a class to manage any responses from out API

## Routes

- Auth routes - anything related to authentication
- Pokemon routes - anything related to the pokemon resource

## Middelware

- Auth - intercept and validate all requests and validate the API token
