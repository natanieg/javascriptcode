const { auth } = require('../models/auth.model');

const allowedRoutes = [
    '/api/v1/auth/generate-token'
];


async function authMiddleware(req, res, next) {
    if (allowedRoutes.includes(req.path)) {
        next();
        return;
    }

    // Extract authorization header
    const { authorization } = req.headers;
    const token = !authorization ? false : authorization.split(' ')[1];

    // Check that the token exists
    if (!token) {
        const apiRes = auth.getUnauthorizedResponse('No Auth token exists', 401);
        res.status(apiRes.status).json(apiRes).end();
        return;
    }


    const tokenResult = await auth.validateToken(token);
    console.log(tokenResult);

    if (tokenResult) {
        next();
    } else {
        const apiRes = auth.getUnauthorizedResponse('Invalid token recieved', 403);
        res.status(apiRes.status).json(apiRes).end();
        return;
    }


    // Verify that the token is valid and active
    //if (token == '12345') {
    //    next();
    //}
    //else {
    //    res.json('Invalid token. ');
    //    return;
    //}
}

module.exports = authMiddleware;