INSERT INTO pokemon (name, species, height, abilities, type, sprite, hp, attack, defense, spattack, spdefense, speed, total, active) 
VALUES ('Pichu', 'Tiny Mouse Pokémon', '0.3 m', 'Static, Lightning Rod', 'ELECTRIC', 'https://img.pokemondb.net/sprites/sun-moon/normal/pichu.png', 20, 40, 15, 35, 35, 60, 205,  1);
