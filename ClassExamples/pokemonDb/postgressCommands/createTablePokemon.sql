CREATE TABLE pokemon
(
    id SERIAL PRIMARY KEY,
    name VARCHAR (80),
    species VARCHAR (40),
    height VARCHAR (10),
    abilities VARCHAR (80),
    type VARCHAR (30),
    sprite VARCHAR (128),
    HP INTEGER,
    attack INTEGER,
    defense INTEGER,
    spAttack INTEGER,
    spDefense INTEGER,
    speed INTEGER,
    total INTEGER,
    active INTEGER DEFAULT 1
);