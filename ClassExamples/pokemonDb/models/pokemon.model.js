const apiResponse = require('./apiResponse');
const { db } = require('./db.model');

class PokemonModel {

    constructor() {
        this.table = 'pokemon';
    }

    async get() {
        const apiRes = new apiResponse();

        try {
            const result = await db.pool.query(`SELECT * FROM ${this.table} WHERE active=1`);
            apiRes.data = result.rows || [];
        } catch (error) {
            apiRes.status = 500;
            apiRes.error = error.message;
        }
        return apiRes;
    }

    async add(pokemon) {
        const apiRes = new apiResponse();

        if (pokemon === null) {
            apiRes.status = 400;
            apiRes.error = 'Please add a pokemon';
            return apiRes;
        }

        console.log(pokemon);

        const placeholder = Object.keys(pokemon).map((key, i) => `$${i + 1}`);
        const columnNames = Object.keys(pokemon).join(',');
        const query = `INSERT INTO ${this.table} (${columnNames}) VALUES (${placeholder.join(',')}) RETURNING id;`
        const insertQuery = {
            text: query,
            values: Object.values(pokemon)
        }

        try {
            const result = await db.pool.query(insertQuery);
            apiRes.data = {
                id: result.rows[0].id,
                ...pokemon
            } || null;

        }
        catch (error) {
            apiRes.status = 500;
            apiRes.error = error.message;
        }
        return apiRes;

    }

    async update(id, pokemon) {
        const apiRes = new apiResponse();

        const updateString = 'UPDATE ' + this.table + ' SET ' + Object.keys(pokemon).map((key, index) => {
            return `${key} = $${index + 1}`
        }).join(',') + ' WHERE id = $' + (Object.keys(pokemon).length + 1);

        const updateQuery = {
            text: updateString,
            values: [...Object.values(pokemon), id]
        }

        try {
            const result = await db.pool.query(updateQuery);
            apiRes.data = {
                ...pokemon
            } || null;

        }
        catch (error) {
            apiRes.status = 500;
            apiRes.error = error.message;
        }
        return apiRes;

    }

    async delete(id) {
        const apiRes = new apiResponse();

        const deleteString = `
        UPDATE ${this.table}
        SET active = 0  
        WHERE id = ${id}
        RETURNING id;`;

        try {
            const result = await db.pool.query(deleteString);
            apiRes.data = {
                id: result.rows[0].id,
            } || null;

        }
        catch (error) {
            apiRes.status = 500;
            apiRes.error = error.message;
        }
        return apiRes;

    }
}

module.exports.pokemonModel = new PokemonModel();