const { Pool } = require('pg');

class Database {
    constructor() {
        this.pool = new Pool({
            database: 'pokemoncatalogue',
            host: 'localhost',
            port: '5432',
            user: 'arcarnan',
            password: '123'
        });
    }
}


module.exports.db = new Database();