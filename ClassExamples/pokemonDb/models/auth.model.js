const { db } = require('./db.model');
const ApiResponse = require('../models/apiResponse');
const crypto = require('crypto');

class Auth {
    // Generate a new token
    async generateToken() {
        const apiRes = new ApiResponse();

        try {
            const newToken = /*await*/ crypto.randomBytes(32).toString('hex');
            const result = await db.pool.query('INSERT INTO authtoken (token, active) VALUES ($1, 1)', [newToken]);

            if (result.rowCount > 0) {
                apiRes.data = {
                    token: newToken
                };
            }
            else {
                throw new Error('Could not generate new token. ');
            }

        } catch (error) {
            apiRes.status = 500;
            apiRes.error = error.message;
        }
        return apiRes;
    }


    // Validate a new token
    async validateToken(token) {
        try {
            const result = await db.pool.query('SELECT * FROM authtoken WHERE active = 1 AND token = $1', [token]);
            return (result.rowCount > 0);
        } catch (error) {

            console.error(error);
            return false;
        }
    }

    getUnauthorizedResponse(error = 'Unauthorized', status) {
        const response = new ApiResponse();
        response.status = status;
        response.error = error;

        return response;
    }
}


module.exports.auth = new Auth();