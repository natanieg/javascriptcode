const express = require('express');
const router = express.Router();

const { auth } = require('../models/auth.model');

router.post('/auth/generate-token', async (req, res) => {

    const apiRes = await auth.generateToken();
    res.status(apiRes.status).json(apiRes).end();
})

module.exports = router;