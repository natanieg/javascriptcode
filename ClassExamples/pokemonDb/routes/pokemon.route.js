const express = require('express');
const router = express.Router();

const { pokemonModel } = require('../models/pokemon.model');


// Get pokemon
router.get('/pokemon', async (req, res) => {
    const pokemonResponse = await pokemonModel.get();
    res.status(pokemonResponse.status).json(pokemonResponse).end();
})

// Add a pokemon
router.post('/pokemon/add', async (req, res) => {
    const { newPokemon } = req.body || null;
    const apiRes = await pokemonModel.add(newPokemon);
    res.status(apiRes.status).send(apiRes).end();
})

// Update a pokemon
router.put('/pokemon/update/:id', async (req, res) => {
    const { newPokemon } = req.body || null;
    const pokeID = req.params.id;
    const apiRes = await pokemonModel.update(pokeID, newPokemon);

    res.status(apiRes.status).send(apiRes).end();
})

// Delete a pokemon
router.delete('/pokemon/delete/:id', async (req, res) => {
    const pokeID = req.params.id;
    const apiRes = await pokemonModel.delete(pokeID);

    res.status(apiRes.status).send(apiRes).end();
})




module.exports = router;