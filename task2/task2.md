# Assignment 2: Post comments
- Link: https://gitlab.com/natanieg/javascriptcode/tree/master/task2

# Assignment text
Use the base provided, or the sample we built during the lecture. 

# INSTRUCTIONS:
- Extend it to redirect to a comments page.
- Remember to add a query parameter "post" with the id of the post. (you will need it to filter the comments)
- Create a comments.html file
- Create a new script file 'comments.js' and do the call for new comments in there
- Extend the Poster object to fetch the comments from the API (See the getPosts method for a reference)
- Display a list of comments that match the post id you added to the link button (comments.html?post=1)
- Make use of the filter() function to find comments for the post
- You can style the comments as you wish, try to be creative, use google fonts have fun!
- BONUS POINTS FOR STYLING ;) 
# OPTIONAL:
- Add a searchbar on the posts page
- Use the onchange event to filter the posts based on the value of the input
- You will need to use some clever variable management to filter out the posts
- I believe in you!
# You can clone the base project from my github:
	- https://github.com/dewaldels/noroff-posts-template.git