// IIFE.
const poster = (function () {
    // private

    //public
    return {
        getPosts() {
            return fetch("https://jsonplaceholder.typicode.com/posts")
                .then(response => response.json());
        },
        createPostTemplate(post) {
            const postDiv = document.createElement("div");
            const postTitle = document.createElement("h4");
            const postBody = document.createElement("p");
            const postLink = document.createElement("a");

            postTitle.innerText = post.title;
            postBody.innerText = post.body;
            postLink.innerText = "View comments";
            postLink.href = `/comments.html?post=${post.id}`;

            postDiv.appendChild(postTitle);
            postDiv.appendChild(postBody);
            postDiv.appendChild(postLink);

            return postDiv;
        }
    }
})();