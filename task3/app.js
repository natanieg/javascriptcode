const order = {
    toppings: [],
    basePrice: 80,
    total: 80
}

const operation = { 'subtract': 0, 'add': 1 };

const toppings = [{
    name: 'pepperoni',
    price: 20
}, {
    name: 'mushroom',
    price: 40
}, {
    name: 'cheese',
    price: 0
}, {
    name: 'pineapple',
    price: 30
}
]

// Cache topping containers
let $pizza = null;
let $mushrooms = null;
let $pepperoni = null;
let $pineapple = null;
let $totalCost = null;

$(document).ready(function () {
    $pizza = $('.pizza');
    $mushrooms = $('.mushrooms');
    $pepperoni = $('.pepperonis');
    $pineapple = $('.pineapples');
    $totalCost = $('#total');

    $('#mushroom').click(function (event) {
        if ($mushrooms.children().length) {
            $mushrooms.empty();
            updateOrder(toppings[1], operation.subtract);
        }
        else {
            $mushrooms.append(generateTopping(toppings[1]));
            updateOrder(toppings[1], operation.add);
        }
    })

    $('#pepperoni').click(function (event) {
        if ($pepperoni.children().length) {
            $pepperoni.empty();
            updateOrder(toppings[0], operation.subtract);
        }
        else {
            $pepperoni.append(generateTopping(toppings[0]));
            updateOrder(toppings[0], operation.add);
        }
    })

    $('#pineapple').click(function (event) {
        if ($pineapple.children().length) {
            $pineapple.empty();
            updateOrder(toppings[3], operation.subtract);
        }
        else {
            $pineapple.append(generateTopping(toppings[3]));
            updateOrder(toppings[3], operation.add);
        }
    })

    $('#cheese').click(function (event) {
        $pizza.toggleClass('no-cheese');
    })
});

// Automatically generate the toppings based on the name and id from the button.
function generateTopping(topping) {
    const looper = Array(10).fill(topping.name);
    return looper.map(item => `<div class="${item}"></div>`).join('');
}

function updateOrder(topping, op) {

    if (op === operation.add) {
        order.toppings.push(topping);
    }
    else {
        order.toppings = order.toppings.filter(top => top != topping);
    }
    order.total = order.basePrice;
    order.toppings.forEach(top => {
        order.total += top.price;
    });
    $totalCost.html(order.total + ".00");
}