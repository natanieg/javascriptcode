const express = require('express');
const router = express.Router();
const pokemonData = require('../models/pokemonModel')

router.get('/', (req, res) => {
    const apiRes = pokemonData.getPokemon();
    res.json(apiRes).end();
});

router.get('/:id', (req, res) => {
    const apiRes = pokemonData.getPokemon(id);
    res.json(apiRes).end();
});

router.post('/add', (req, res) => {
    res.send('Adding pokemon');
})

router.put('/update/:id', (req, res) => {
    res.send('Adding pokemon');
})

router.delete('/delete/:id', (req, res) => {
    res.send('Adding pokemon');
})


module.exports = router;