const apiResponse = require('./api-response');
const test = require('./test.json');

class PokemonModel {


    getPokemon() {
        const apiRes = new apiResponse();
        apiRes.setData(test);
        return apiRes;
    }
}

module.exports = new PokemonModel();