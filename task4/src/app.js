const test = require('./models/test.json');
const express = require('express');
const app = express();
const PORT = process.env.PORT || 3333;
const pokemonRoutes = require('./routes/pokemon');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get('/example', (req, res) => {
    console.log(test);

    //let dataPokemon = JSON.parse(test);
    return res.status(200).send(test).end();
});

app.use('/api/v1/pokemon', pokemonRoutes);


// Retrieve an array of pokemon
//app.get('/api/v1/pokemon', (req, res) => {
//    res.status(200).send("Hello world").end();
//});
//
//// Retrieve a pokemon
//app.get('/api/v1/pokemon/:id', (req, res) => {
//    const itemId = req.params.id;
//    res.status(200).send("Hello world").end();
//});
//
//// Add a new pokemon 
//app.post('/api/v1/pokemon/add', (req, res) => {
//const result = {
//    ...req.body,
//    address: {
//        street: "123 street",
//        city: "city city"
//    }
//}
//console.log(req.body);
//
//res.status(200).send(result).end();
//return res.json(req.body);
//    return res.status(200).send('Received a POST HTTP method').end();
//})
//
//// Update a pokemon based on the pokemonId
//app.put('/api/v1/pokemon/update/:id', (req, res) => {
//    const itemId = req.params.id;
//    return res.status(200).send('Received a PUT HTTP method').end();
//})
//
//// Remove a pokemon based on the pokemonId
//app.delete('/api/v1/pokemon/delete/:id', (req, res) => {
//    const itemId = req.params.id;
//    return res.status(200).send('Received a DELETE HTTP method').end();
//})
//
//
app.listen(PORT, (e) => {
    if (e) {
        throw new Error('Internal Server Error');
    }
    console.log(`Server running on: ${PORT}`);
});