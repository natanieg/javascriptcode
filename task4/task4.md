# Assignment 4: Pokemon API, part 1

- Link: https://gitlab.com/natanieg/javascriptcode/tree/master/task4

# Assignment text

Build a node application with the following endpoints:

- [ GET ] /api/v1/pokemon - Retrieve an array of pokemon
- [ POST ] /api/v1/pokemon/add - Add a new pokemon
- [ PUT ] /api/v1/pokemon/update - Update a pokemon based on the pokemonId
- [ DELETE ] /api/v1/pokemon/delete - Remove a pokemon based on the pokemonId

# NOTE:

- You do not have a database yet so you can't actually add, update or delete Pokemon. This task is simply to start planning the API and scaffolding the code we will be building this week.
